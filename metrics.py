# How to run:
# python metrics.py

import os
import json
import urllib2
from datetime import datetime
from datetime import timedelta

git_time_format = '%Y-%m-%dT%H:%M:%S.%fZ'
today = str(datetime.now().date())

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
RAW_DIR = os.path.join(BASE_DIR, 'raw_' + today)
RESULTS_DIR = os.path.join(BASE_DIR, 'results_' + today)

GITLAB_API_URL = 'https://gitlab.com/api/v4/projects/'

COMMITS = 'commits'
LABELS = 'labels'
MILESTONES = 'milestones'
ISSUES = 'issues'
NOTES = 'notes'
EXT = '.json'

commits = None
labels = None
issues = None
notes = None
milestones = None


def collect_data(data_type, project_id, token, issue_no=None):
    if data_type is not NOTES:
        print('collecting ', data_type, ' data...')

    url = ''
    result = []
    if data_type is ISSUES:
        url = GITLAB_API_URL + project_id + '/issues?per_page=100'
    elif data_type is NOTES:
        url = GITLAB_API_URL + project_id + '/issues/' + str(issue_no) + '/notes?per_page=100'
    elif data_type is LABELS:
        url = GITLAB_API_URL + project_id + '/labels?per_page=100'
    elif data_type is COMMITS:
        url = GITLAB_API_URL + project_id + '/repository/commits?per_page=100'
    elif data_type is MILESTONES:
        url = GITLAB_API_URL + project_id + '/milestones?per_page=100'

    request = urllib2.Request(url, headers={'Private-Token': token})
    response = urllib2.urlopen(request)
    r = response.read()
    result.extend(json.loads(r.decode("utf-8")))

    if int(response.info().getheader('X-Total-Pages')) > 1:
        for i in range(2, int(r.headers['X-Total-Pages']) + 1):
            new_url = url + '&page=' + str(i)
            request = urllib2.Request(new_url, headers={'Private-Token': token})
            response = urllib2.urlopen(request)
            r = response.read()
            result.extend(json.loads(r.decode("utf-8")))

    # write json file
    if data_type is not NOTES:
        output = data_type + EXT
        f = open(os.path.join(RAW_DIR, output), 'w')
        try:
            json.dump(result, f, indent=4)
        finally:
            f.close()
    return result


def collect_all_data(project_id, token):
    # Grab commits
    global commits
    global labels
    global issues
    global notes
    global milestones
    commits = collect_data(COMMITS, project_id, token)
    labels = collect_data(LABELS, project_id, token)
    milestones = collect_data(MILESTONES, project_id, token)
    issues = collect_data(ISSUES, project_id, token)
    notes = []

    print('collecting notes data...')
    for issue in issues:
        notes.append(collect_data(NOTES, project_id, token, issue['iid']))
    save_data_to_disk(notes, RAW_DIR, NOTES + EXT)
    pass


def save_data_to_disk(text, folder, output):
    f = open(os.path.join(folder, output), 'w')
    try:
        json.dump(text, f, indent=4)
    finally:
        f.close()


def generate_wip():
    """Generate the work in progress metric by the number of issues
    in the pipeline
    :returns the total work in progress"""
    total = 0
    per_label = {}
    result = {}

    print('generating WIP metrics...')
    for issue in issues:
        if issue['state'] == 'opened':
            total = total + 1
            for label in issue['labels']:
                if label in per_label:
                    per_label[label] = per_label[label] + 1
                else:
                    per_label[label] = 1

    result['total'] = total
    result['per_label'] = per_label
    save_data_to_disk(result, RESULTS_DIR, 'wip' + EXT)
    return total


def generate_lead_time(s_label):
    """Geneates the lead time on every issue using the issue notes to identify the date
    time change when added as in development to closed
    :arg s_label - the string of the label that defines the start of work
    :returns the avearge leadtime"""

    result = {}
    per_issue = []
    lead_times = []
    label_id = None
    print('generating Lead Time metrics...')
    # find the id for the label identified as start of work
    for label in labels:
        if label['name'] == s_label:
            label_id = label['id']

    # calculate lead time of each issue
    for issue in issues:
        details = {}
        issue_no = issue['iid']
        details['issue_no'] = issue_no
        assignees = str(issue['assignees'])
        start_time = None
        closed_time = None

        for note in notes:
            for entry in note:
                if 'closed' in entry['body'] and entry['noteable_iid'] == issue_no:
                    closed_time = entry['created_at']
                if 'added ~' + str(label_id) + ' label' in entry['body'] and entry['noteable_iid'] == issue_no:
                    start_time = entry['created_at']

        if start_time is not None and closed_time is not None:
            details['issue_no'] = issue_no
            start_time = datetime.strptime(start_time, git_time_format)
            closed_time = datetime.strptime(closed_time, git_time_format)
            lead_time = closed_time - start_time
            lead_times.append(lead_time)
            details['lead_time'] = str(lead_time)
            details['assignees'] = assignees
            per_issue.append(details)

    # calculate the avg lead times
    if len(lead_times) == 0:
        avg = 0
    else:
        avg = timedelta()
        for time in lead_times:
            avg = avg + time
        avg = avg / len(lead_times)

    result['average_time'] = str(avg)
    result['per_issue'] = per_issue
    save_data_to_disk(result, RESULTS_DIR, 'lead_time' + EXT)
    return avg


def generate_development_time(s_label):
    """Generates the development time data on an issue"""

    result = {}
    per_issue = []
    development_times = []
    print('generating Development Time metrics...')
    # find the id for the label identified as start of work
    for label in labels:
        if label['name'] == s_label:
            label_id = label['id']

    # calculate development time of each issue
    for issue in issues:
        details = {}
        issue_no = issue['iid']
        details['issue_no'] = issue_no
        assignees = str(issue['assignees'])
        start_time = None
        end_time = None

        for note in notes:
            for entry in note:
                if ('added ~' + str(label_id)) in entry['body'] and entry['noteable_iid'] == issue_no:
                    start_time = entry['created_at']
                if ('removed ~' + str(label_id)) in entry['body'] and entry['noteable_iid'] == issue_no:
                    end_time = entry['created_at']

        # if start_time is still none the work was never started use today's date as start_time
        # this can generate bogus results if a user renames the start label
        if start_time is not None and end_time is not None:
            start_time = datetime.strptime(start_time, git_time_format)
            end_time = datetime.strptime(end_time, git_time_format)
            development_time = end_time - start_time
            development_times.append(development_time)
            details['development_time'] = str(development_time)
            details['assignees'] = assignees
            per_issue.append(details)

    # calculate the avg development time
    if len(development_times) == 0:
        avg = 0
    else:
        avg = timedelta()
        for time in development_times:
            avg = avg + time
        avg = avg / len(development_times)

    result['average_time'] = str(avg)
    result['per_issue'] = per_issue
    save_data_to_disk(result, RESULTS_DIR, 'development_time' + EXT)
    return avg


def generate_wasteage_time(s_label):
    """Generates the wasteage data on an issue"""

    result = {}
    per_issue = []
    wasteage_times = []
    label_id = None
    print('generating Wasteage Time metrics...')
    # find the id for the label identified as start of work
    for label in labels:
        if label['name'] == s_label:
            label_id = label['id']

    # calculate wastage time of each issue
    for issue in issues:
        details = {}
        issue_no = issue['iid']
        assignees = str(issue['assignees'])
        details['issue_no'] = issue_no
        start_time = None
        created_time = issue['created_at']

        for note in notes:
            for entry in note:
                if ('added ~' + str(label_id)) in entry['body'] and entry['noteable_iid'] == issue_no:
                    start_time = entry['created_at']

        # if start_time is still none the work was never started use today's date as start_time
        # this can generate bogus results if a user renames the start label
        if start_time is not None:
            start_time = datetime.strptime(start_time, git_time_format)
            created_time = datetime.strptime(created_time, git_time_format)
            wasteage_time = start_time - created_time
            wasteage_times.append(wasteage_time)
            details['wastage_time'] = str(wasteage_time)
            details['assignees'] = assignees
            per_issue.append(details)

    # calculate the avg wastage time
    if len(wasteage_times) == 0:
        avg = 0
    else:
        avg = timedelta()
        for time in wasteage_times:
            avg = avg + time
        avg = avg / len(wasteage_times)

    result['average_time'] = str(avg)
    result['per_issue'] = per_issue
    save_data_to_disk(result, RESULTS_DIR, 'wastage_time' + EXT)
    return avg


def generate_throughput(wip, avg_lead_time):
    print('generating throughput metrics...')
    time_in_seconds = avg_lead_time.total_seconds()
    balancing_factor = 10 ** 6
    throughput = {'project_throughput': ((wip / time_in_seconds) * balancing_factor)}
    save_data_to_disk(throughput, RESULTS_DIR, 'throughput' + EXT)
    pass


def generate_flow_efficiency(avg_development_time, avg_lead_time):
    print('generating flow efficiency metrics...')
    avg_development_time = avg_development_time.total_seconds()
    avg_lead_time = avg_lead_time.total_seconds()
    throughput = {'project_flow_efficiency': (avg_development_time / avg_lead_time)}
    save_data_to_disk(throughput, RESULTS_DIR, 'flow_efficiency' + EXT)
    pass


def generate_commits_per_person():
    """Generate the commits per person metric by the number
    of commits in the repository"""

    commits_per_person = {}
    result = {}
    print('generating Commits Per Person metrics...')
    for commit in commits:
        author_name = commit['author_name']
        if author_name in commits_per_person:
            commits_per_person[author_name] = commits_per_person[author_name] + 1
        else:
            commits_per_person[author_name] = 1

    result['commits_per_person'] = commits_per_person
    save_data_to_disk(result, RESULTS_DIR, 'commits_per_person' + EXT)
    pass


def generate_burn_down_chart():
    """Generate a graphical burn down chart by the issues opened and the milestones"""


def do_analysis(project_id, token, start_label=None):
    if not os.path.exists(RAW_DIR):
        os.makedirs(RAW_DIR)
    if not os.path.exists(RESULTS_DIR):
        os.makedirs(RESULTS_DIR)
    print('*collecting data*')
    collect_all_data(project_id, token)

    wip = generate_wip()
    avg_lead_time = generate_lead_time(start_label)
    generate_throughput(wip, avg_lead_time)

    avg_development_time = generate_development_time(start_label)
    avg_wastage_time = generate_wasteage_time(start_label)
    generate_flow_efficiency(avg_development_time, avg_lead_time)

    generate_commits_per_person()
    generate_burn_down_chart()
    print('Successfully completed. See results_' + today + '/')


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--project_id", help="REQUIRED: The project id for your gitlab project")
    parser.add_argument("-t", "--token", help="REQUIRED: Private token for gitlab instance")
    parser.add_argument("-s", "--start_label", help="REQUIRED: specify the label used as the first development stage")
    parser.add_argument("-i", "--instance",
                        help="optional: specify the location of the gitlab instance default: gitlab.com")
    args = parser.parse_args()

    if args.instance is not None:
        GITLAB_API_URL = 'http://' + args.instance + '/api/v4/projects/'

    print('Script using: ', GITLAB_API_URL)
    if args.project_id is not None and args.token is not None and args.start_label is not None:
        do_analysis(args.project_id, args.token, args.start_label)
    else:
        raise BaseException("See docs for REQUIRED. Type 'python metrics.py -h' for further help")
