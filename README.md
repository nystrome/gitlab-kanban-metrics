## How to use this script

### Script Parameters
`-p` Specifies the Project ID

`-t` Specifies your Private Token to access the api

`-s` Specifies the name of the label used to identify that work has been moved from the backlog into development

`-i` Specifies the server location of your gitlab instance. If empty the script will use gitlab.com

### Getting your Project ID
* Go to project
* Go to Settings -> General
* Expand General project settings
* `copy` Project ID

### Getting your Private Token from GitLab instances
* Click profile avatar in top right corner
* Go to profile settings
* Go to access tokens in the left sidebar navigation
* In Personal Access Token
    - Select a name
    - Set optional expiry date
    - Set api for scope
    - Click `create personal access token`
    - Save the result*
    
*The result cannot be accessed again


### Running the script
e.g. `python metrics.py -p "23415352" -t "J89Pqnsssssss3o_xsC1" -i "130.124.251.333" -s "In Progress"`